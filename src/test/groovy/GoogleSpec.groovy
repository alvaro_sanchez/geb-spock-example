import geb.spock.GebReportingSpec
import org.openqa.selenium.Keys

class GoogleSpec extends GebReportingSpec {

    def "the first link should be wikipedia"() {
        when:
        to HomePage

        and:
        searchField.value "wikipedia"
        searchField << Keys.chord(Keys.ENTER)

        then:
        at SearchResultsPage

        and:
        resultLink(0).text() == "Wikipedia"

        when:
        resultLink(0).click()

        then:
        waitFor { at WikipediaPage }
    }
}

import geb.Page

class SearchResultsPage extends Page {

    static at = { results }

    static content = {
        results(wait: true) { $("li.g") }
        result { index -> results[index] }
        resultLink { index -> result(index).find("h3.r > a") }
    }

}

import geb.Page

class HomePage extends Page {

    static url = "http://google.com/ncr"

    static at = { title == "Google" }

    static content = {
        searchField { $("input[name=q]") }
        searchButton { $("input[name='btnG']") }
    }

}

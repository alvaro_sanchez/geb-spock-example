# Geb/Spock Sample Project

This is an example of how to get started using [Geb](http://www.gebish.org/) and [Spock](http://spockframework.org) 
for functional tests in a [Gradle](http://www.gradle.org/)-based standalone project.

The project uses the Gradle wrapper, so you don't need to install it. Just checkout the project and run `./gradlew test`.

It is also integrated with [PhantomJS](http://phantomjs.org/), so you must install it beforehand 
(if using Homebrew on Mac, just `brew install phantomjs`).